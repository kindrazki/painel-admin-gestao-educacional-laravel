<?php

namespace SON\Models\Http\Controllers;

use SON\Models\SONModelsUser;
use Illuminate\Http\Request;

class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \SON\Models\SONModelsUser  $sONModelsUser
     * @return \Illuminate\Http\Response
     */
    public function show(SONModelsUser $sONModelsUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SON\Models\SONModelsUser  $sONModelsUser
     * @return \Illuminate\Http\Response
     */
    public function edit(SONModelsUser $sONModelsUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \SON\Models\SONModelsUser  $sONModelsUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SONModelsUser $sONModelsUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SON\Models\SONModelsUser  $sONModelsUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(SONModelsUser $sONModelsUser)
    {
        //
    }
}
